describe 'Feature de exemplo Kafka' do
  extend AllureStepAnnotation

  context 'Postar payloads no Schedule Orchestrator' do |_example|
    it 'Postar mensagem no tópico schedule-orchestrator-agendamento-hml', tms: 'QA-122', severity: :normal do |example|
      example.run_step('Pre-Requisites') do
        kafka_client = initialize_kafka_client('dasa-broker-hml-central-us.servicebus.windows.net:9093')
        Allure.add_attachment(
          name: 'Pré requisitos para o teste ser executado',
          source: kafka_client.inspect,
          type: Allure::ContentType::TXT
        )
      end
      binding.pry
      # client = Mysql2::Client.new(:host => "10.150.4.14", :username => "cronos_admin@cronos-postgres-server", :password => ' ultr@ss0n', :database => 'schedule_event')
      # results = client.query("SELECT event_id, database_id, event_situation, retry_attempt, created FROM public.schedule_event_retry_log WHERE event_id = 12046359")


      kafka_client = initialize_kafka_client('dasa-broker-hml-central-us.servicebus.windows.net:9093')
      file = File.read('spec\support\data\AgOnline-Payload.json')
      # object = JSON.parse(file, object_class: OpenStruct)
      kafka_client.deliver_message('Hello World', topic: 'schedule-orchestrator-agendamento-hml')
      Allure.add_attachment(
        name: 'Payload usado no teste',
        source: file,
        type: Allure::ContentType::JSON
      )

      example.run_step('Assert') do
        expect(true)
      end
    end
  end
end
