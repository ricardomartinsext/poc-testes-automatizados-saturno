require 'allure-rspec'
require 'yaml'
require 'ruby-kafka'
# require 'mysql2'
require 'rspec'
require 'pry'
require 'kafka'
require 'json'

require_all 'spec/support'

AllureRspec.configure do |c|
  c.results_directory = 'report/allure'
  c.clean_results_directory = true
  c.link_tms_pattern = 'https://example.org/tms/{}'
  c.link_issue_pattern = 'https://example.org/issue/{}'
end

RSpec.configure do |config|
  config.before(:each) do |_example|
    Allure.step(name: 'Global before step') do
      Allure.add_attachment(
        name: 'Global test attachment',
        source: 'Attachment',
        type: Allure::ContentType::TXT,
        test_case: true
      )
      expect(false)
    end
  end

  config.after(:each) do |_example|
    Allure.step(name: 'Global after step') do
      expect(true)
    end
  end
end
